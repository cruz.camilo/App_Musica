<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Buscar..."/>
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">

            <!-- Optionally, you can add icons to the links -->
            <li><a href="../profesor/inicio.jsp"><i class="fa fa-link"></i> <span>Vista como Profesor</span></a></li>
            <li><a href="../estudiante/inicio.jsp"><i class="fa fa-link"></i> <span>Anuncios</span></a></li>          
            <li><a href="../estudiante/info_curso.jsp"><i class="fa fa-link"></i> <span>Acerca del Curso</span></a></li>
            <li><a href="../estudiante/info_profesor.jsp"><i class="fa fa-link"></i> <span>Acerca del profesor</span></a></li>
            <li><a href="../estudiante/lecciones.jsp"><i class="fa fa-link"></i> <span>Lecciones</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>Diario de Práctica</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="../diario/practicar.jsp">Practicar</a></li>
                    <li><a href="../diario/registros_pasados.jsp">Registros pasados</a></li>
                    <li><a href="">Programa tu práctica</a></li>
                    <li><a href="#">Metronomo</a></li>
                    <li><a href="#">Compartir</a></li>                                                             
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>Evaluaciones</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu" style="display: none;">
                    <li><a href="../estudiante/mis_evaluaciones.jsp">Evaluaciones Activas</a></li>
                    <li><a href="../estudiante/mis_calificaciones.jsp">Resultados</a></li>                                
                </ul>
            </li>
            <li><a href="#"><i class="fa fa-link"></i> <span>Foro</span></a></li>            
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>