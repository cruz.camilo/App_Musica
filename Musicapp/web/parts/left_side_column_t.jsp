<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Buscar..."/>
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">

            <!-- Optionally, you can add icons to the links -->
            <li><a href="../estudiante/inicio.jsp"><i class="fa fa-link"></i> <span>Vista como Estudiante</span></a></li>
            <li><a href="../profesor/publicar_anuncio.jsp"><i class="fa fa-link"></i> <span>Publicar un Anuncio</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>Informaci�n Sobre El Curso</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu" style="display: none;">
                    <li><a href="../profesor/crear_curso.jsp">Crear Curso</a></li>
                    <li><a href="../profesor/acerca_curso.jsp">Acerca del Curso</a></li>
                    <li><a href="../profesor/editar_profesor.jsp">Acerca del Profesor</a></li>                                
                </ul>
            </li>
            <li><a href="../estudiante/lecciones.jsp"><i class="fa fa-link"></i> <span>Lecciones</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>Diario de Pr�ctica</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" style="display: none;">
                    <li><a href="#">Recomendaciones</a></li>
                    <li><a href="../diario/registros_pasados.jsp">Registros pasados</a></li>
                    <li><a href="../diario/programa_practica.jsp">Programa tu pr�ctica</a></li>
                    <li><a href="#">Metr�nomo</a></li>
                    <li><a href="#">Compartir</a></li>                                                             
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>Evaluaciones</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu" style="display: none;">
                    <li><a href="">Publicar una Evaluaci�n</a></li>
                    <li><a href="">Calificar Evaluaciones</a></li>                                
                </ul>
            </li>
            <li><a href="#"><i class="fa fa-link"></i> <span>Foro</span></a></li>            
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>