/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import bean.CursoBean;
import entity.Archivo;
import entity.Curso;
import entity.Evaluacion;
import entity.Fragmento;
import entity.Leccion;
import entity.Publicacion;
import entity.RegistroDiario;
import entity.Usuario;
import entity.Resultado;
import java.io.File;
import java.sql.Timestamp;
import java.time.LocalDate;
import model.CursoModel;
import org.hibernate.Session;
import util.HibernateUtil;

public class Main {

    private Session session;

    public static void main(String[] args) {

        Session session = HibernateUtil.getSessionFactory().openSession(); //Ejecuta todo el código correspondiente a crear la base de datos.

        //u.setFecha_registro(new Timestamp(System.currentTimeMillis()));
        //Evaluacion e = new Evaluacion("practica", "prueba", "blaaa", "guitarra", u);
        //e.setFecha_publicacion(new Timestamp(System.currentTimeMillis()));        
        //Resultado r = new Resultado(5, "Aprobo", "Muy bien!", e);
        //Fragmento f = new Fragmento("probando", "prueba", "es una prueba", "practica");
        //Short t = new Short("15");
        //Short b = new Short("130");
        //RegistroDiario r = new RegistroDiario(b, t, "muy bien", f);
        //Usuario u = new Usuario("Luisa", "lfurazan@gmail.com", "4321", "estudiante");
        //Fragmento g = new Fragmento("maiden", "alternate picking", "intro", "repertorio", "iron maiden", u);
        //RegistroDiario r = new RegistroDiario(b, t, "comentarios", g);
        /*session.beginTransaction();
        Archivo p = new Archivo("probando", "hola");
        session.persist(p);
        
        
        session.getTransaction().commit();
        session.close();*/
 /*
        CODIGO PARA ENVIAR CORREO
        
        final String username = "ccamiloeduardo@gmail.com";
        final String password = "jacksonteamo1804";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("ccamiloeduardo@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("ccamiloeduardo@gmail.com"));
            message.setSubject("Testing Subject");
            message.setText("Dear Mail Crawler,"
                    + "\n\n No spam to my email, please!");

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
         */
        File file = new File("/Users/pankaj/java.txt");
        System.out.println("File extension is: " + getFileExtension(file));
    }

    private static String getFileExtension(File file) {
        String fileName = file.getName();
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        } else {
            return "";
        }
    }
}
