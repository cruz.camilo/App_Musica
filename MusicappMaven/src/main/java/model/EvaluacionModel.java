/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Evaluacion;
import java.sql.Timestamp;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.hibernate.Query;
import org.hibernate.Session;
import util.HibernateUtil;

@ManagedBean
@RequestScoped
public class EvaluacionModel {
    
    private Session session;
    private List<Evaluacion> evaluaciones;
    
    public List<Evaluacion> listarEvaluaciones() {
        session = HibernateUtil.getSessionFactory().openSession();
        
        Query query = session.createQuery("FROM Evaluacion");
        
        this.evaluaciones = (List<Evaluacion>) query.list();
        
        return this.evaluaciones;
    }
    
    public void registrarEvaluacion(Evaluacion e) {
        
        session = HibernateUtil.getSessionFactory().openSession();
        
        session.getTransaction().begin();
        e.setFechaPublicacion(new Timestamp(System.currentTimeMillis()));
        session.persist(e);
        session.getTransaction().commit();
        
        session.close();
    }
    
    public void eliminarEvaluacion(Evaluacion e) {
        session = HibernateUtil.getSessionFactory().openSession();
        session.getTransaction().begin();
        session.delete(e);
        session.getTransaction().commit();
        session.close();
    }
    
}
