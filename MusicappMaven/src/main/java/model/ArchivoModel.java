/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Archivo;
import util.HibernateUtil;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import java.util.ArrayList;


public class ArchivoModel {

    private Session session;

    
    public void registrarArchivo(Archivo archivoNuevo) {
        
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            archivoNuevo.setFecha_registro(new Timestamp(System.currentTimeMillis()));           
            session.persist(archivoNuevo);

            session.getTransaction().commit();
            session.close();
        
    }

    public List listarArchivo() {
        List<Archivo> archivos = new ArrayList();
        try {
            session = HibernateUtil.getSessionFactory().openSession();

            String hql = "FROM Archivo";
            //String hql = "FROM Archivo WHERE tipo = 'estudiante'";
            Query query = session.createQuery(hql);

            System.out.println(query.list());

            archivos = (List<Archivo>) query.list();

        } catch (Exception e) {
            throw e;
        }

        System.out.println(archivos);
        return archivos;
    }

    public Archivo updateArchivo(Archivo archivo) {
        Archivo udb = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            udb = (Archivo) session.get(Archivo.class, archivo.getCodigo());

            udb.setRecurso(archivo.getRecurso());
            udb.setDescripcion(archivo.getDescripcion());
            
            session.save(udb);

            session.getTransaction().commit();

            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return udb;
    }
}