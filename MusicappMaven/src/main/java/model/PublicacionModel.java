package model;

import entity.Publicacion;
import java.sql.Timestamp;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.hibernate.Query;
import org.hibernate.Session;
import util.HibernateUtil;

@ManagedBean
@RequestScoped
public class PublicacionModel {
    
    private Session session;
    private List<Publicacion> publicaciones;
    
    public List<Publicacion> listarPublicaciones() {
        session = HibernateUtil.getSessionFactory().openSession();
        
        Query query = session.createQuery("FROM Publicacion");
        
        this.publicaciones = (List<Publicacion>) query.list();
        
        return this.publicaciones;
    }
    
    public void registrarPublicacion(Publicacion p) {
        
        session = HibernateUtil.getSessionFactory().openSession();
        
        session.getTransaction().begin();
        p.setFechaPublicacion(new Timestamp(System.currentTimeMillis()));
        session.persist(p);
        session.getTransaction().commit();
        
        session.close();
    }
    
    public void eliminarPublicacion(Publicacion p) {
        session = HibernateUtil.getSessionFactory().openSession();
        session.getTransaction().begin();
        session.delete(p);
        session.getTransaction().commit();
        session.close();
    }
}
