/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


import entity.Fragmento;
import entity.Usuario;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import org.hibernate.Query;
import org.hibernate.Session;
import util.HibernateUtil;


public class FragmentoModel {
    
    private Session session;
    private Fragmento fragmento = new Fragmento();
    
    public Fragmento getFragmento() {
        return fragmento;
    }

    public void setFragmento(Fragmento fragmento) {
        this.fragmento = fragmento;
    }

    public void registrarFragmento(Fragmento fragmentoNuevo) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.persist(fragmentoNuevo);
            session.getTransaction().commit();
            session.close();

        } catch (Exception e) {
            throw e;
        }
    }
    
     public List listarFragmento() {
        List<Fragmento> cursos = new ArrayList();
        fragmento.setUsuario(
                (Usuario) FacesContext.
                getCurrentInstance().
                getExternalContext()
                .getSessionMap().
                get("udentro"));
        
        try {
            session = HibernateUtil.getSessionFactory().openSession();

            String hql = "FROM Fragmento WHERE usuario = :fragmento";
            Query query = session.createQuery(hql);
            query.setParameter("fragmento", fragmento.getUsuario());

            System.out.println(query.list());

            cursos = (List<Fragmento>) query.list();

        } catch (Exception e) {
            throw e;
        }

        System.out.println(cursos);
        return cursos;
    }
     
      public void eliminarFragmento(Fragmento f) {
        session = HibernateUtil.getSessionFactory().openSession();
        session.getTransaction().begin();
        session.delete(f);
        session.getTransaction().commit();
        session.close();
    }
}