/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Curso;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import util.HibernateUtil;

/**
 *
 * @author User
 */
public class CursoModel {

    private Session session;

    public void registrarCurso(Curso cursoNuevo) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            cursoNuevo.setFecha(new Timestamp(System.currentTimeMillis()));

            session.persist(cursoNuevo);

            session.getTransaction().commit();

            session.close();

        } catch (Exception e) {
            throw e;
        }
    }

    public List listarCurso() {
        List<Curso> cursos = new ArrayList();
        try {
            session = HibernateUtil.getSessionFactory().openSession();

            String hql = "FROM Curso";
            Query query = session.createQuery(hql);

            System.out.println(query.list());

            cursos = (List<Curso>) query.list();

        } catch (Exception e) {
            throw e;
        }

        System.out.println(cursos);
        return cursos;
    }
    
        public Curso updateCurso(Curso curso) {
        Curso cdb = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            cdb = (Curso) session.get(Curso.class, curso.getId());

            cdb.setNombre(curso.getNombre());
            cdb.setDescripcion(curso.getDescripcion());
            cdb.setAcercaProfesor(curso.getAcercaProfesor());
            
            session.save(cdb);

            session.getTransaction().commit();

            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cdb;
    }
}