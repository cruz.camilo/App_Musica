/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Resultado;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import util.HibernateUtil;


public class ResultadoModel {
private Session session;
    private List<Resultado> resultados;
    
    public List<Resultado> listarResultadoes() {
        session = HibernateUtil.getSessionFactory().openSession();
        
        Query query = session.createQuery("FROM Resultado");
        
        this.resultados = (List<Resultado>) query.list();
        
        return this.resultados;
    }
    
    public void registrarResultado(Resultado r) {
        
        session = HibernateUtil.getSessionFactory().openSession();        
        session.getTransaction().begin();
        session.persist(r);
        session.getTransaction().commit();
        
        session.close();
    }
    
    public void eliminarResultado(Resultado r) {
        session = HibernateUtil.getSessionFactory().openSession();
        session.getTransaction().begin();
        session.delete(r);
        session.getTransaction().commit();
        session.close();
    }    
}
