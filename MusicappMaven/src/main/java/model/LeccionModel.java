/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Leccion;
import java.sql.Timestamp;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import util.HibernateUtil;

public class LeccionModel {
    private Session session;
    private List<Leccion> lecciones;
    
    public List<Leccion> listarLecciones() {
        session = HibernateUtil.getSessionFactory().openSession();
        
        Query query = session.createQuery("FROM Leccion");
        
        this.lecciones = (List<Leccion>) query.list();        
        return this.lecciones;
    }
    
    public void registrarLeccion(Leccion e) {
        
        session = HibernateUtil.getSessionFactory().openSession();
        
        session.getTransaction().begin();
        e.setFechaPublicacion(new Timestamp(System.currentTimeMillis()));
        session.persist(e);
        session.getTransaction().commit();
        
        session.close();
    }
    
    public void eliminarLeccion(Leccion l) {
        session = HibernateUtil.getSessionFactory().openSession();
        session.getTransaction().begin();
        session.delete(l);
        session.getTransaction().commit();
        session.close();
    }
}
