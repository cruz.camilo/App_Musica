/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import util.HibernateUtil;
import entity.Usuario;
import java.sql.Timestamp;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import java.util.ArrayList;


public class UsuarioModel {

    private Session session;

    public Usuario verificarDatos(Usuario usuario) throws Exception {
        Usuario us = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();

            String hql = "FROM Usuario WHERE correo = '" + usuario.getCorreo()
                    + "' and clave = '" + usuario.getClave() + "'";
            Query query = session.createQuery(hql);

            if (!query.list().isEmpty()) {
                us = (Usuario) query.list().get(0);
            }

        } catch (Exception e) {
            throw e;
        }

        return us;
    }

    public void registrarUsuario(Usuario usuarioNuevo) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            usuarioNuevo.setFecha_registro(new Timestamp(System.currentTimeMillis()));
            usuarioNuevo.setFoto("anonimo.png");
            session.persist(usuarioNuevo);

            session.getTransaction().commit();

            session.close();

        } catch (Exception e) {
            throw e;
        }
    }

    public List listarUsuario() {
        List<Usuario> usuarios = new ArrayList();
        try {
            session = HibernateUtil.getSessionFactory().openSession();

            String hql = "FROM Usuario";
            //String hql = "FROM Usuario WHERE tipo = 'estudiante'";
            Query query = session.createQuery(hql);

            System.out.println(query.list());

            usuarios = (List<Usuario>) query.list();

        } catch (Exception e) {
            throw e;
        }

        System.out.println(usuarios);
        return usuarios;
    }

    public Usuario updateUser(Usuario user) {
        Usuario udb = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            udb = (Usuario) session.get(Usuario.class, user.getCodigo());

            udb.setNombre(user.getNombre());
            udb.setCorreo(user.getCorreo());
            udb.setClave(user.getClave());
            udb.setFoto(user.getFoto());
            udb.setAcercaDeMi(user.getAcercaDeMi());

            session.save(udb);

            session.getTransaction().commit();

            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return udb;
    }
}