/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;



import entity.EvaluacionEstudiante;
import util.HibernateUtil;

import java.sql.Timestamp;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import java.util.ArrayList;


public class EvaluacionEstudianteModel {

    private Session session;

    
    public void registrarEvaluacion(EvaluacionEstudiante evaluacionNueva) {
        
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            evaluacionNueva.setFecha_registro(new Timestamp(System.currentTimeMillis()));           
            session.persist(evaluacionNueva);

            session.getTransaction().commit();
            session.close();
        
    }

    public List listarEvaluacion() {
        List<EvaluacionEstudiante> evaluaciones = new ArrayList();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            //"FROM EvaluacionEstudiante INNER JOIN Usuario"
            String hql = "FROM EvaluacionEstudiante";           
            Query query = session.createQuery(hql);

            System.out.println(query.list());

            evaluaciones = (List<EvaluacionEstudiante>) query.list();

        } catch (Exception e) {
            throw e;
        }

        System.out.println(evaluaciones);
        return evaluaciones;
    }

}