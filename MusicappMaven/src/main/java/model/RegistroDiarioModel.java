/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Fragmento;
import entity.RegistroDiario;
import entity.Usuario;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import org.hibernate.Query;
import org.hibernate.Session;
import util.HibernateUtil;


public class RegistroDiarioModel {
    private Session session;
    
    private Fragmento fragmento = new Fragmento();
    
     public Fragmento getFragmento() {
        return fragmento;
    }

    public void setFragmento(Fragmento fragmento) {
        this.fragmento = fragmento;
    }

    
    public List<RegistroDiario> listarRegistroDiario() {
        List<RegistroDiario> entradasDiario = new ArrayList();;
        
        fragmento.setUsuario(
                (Usuario) FacesContext.
                getCurrentInstance().
                getExternalContext()
                .getSessionMap().
                get("udentro"));
        
        try {
            session = HibernateUtil.getSessionFactory().openSession();

            String hql = "FROM RegistroDiario WHERE usuario = :registro";
            Query query = session.createQuery(hql);
            query.setParameter("registro", fragmento.getUsuario());

            System.out.println(query.list());

            entradasDiario = (List<RegistroDiario>) query.list();

        } catch (Exception e) {
            throw e;
        }
                
        return entradasDiario;
    }
    
    public void registrarRegistroDiario(RegistroDiario r) {
        
        session = HibernateUtil.getSessionFactory().openSession();
        
        session.getTransaction().begin();
        r.setFecha_registro(new Timestamp(System.currentTimeMillis()));
        session.persist(r);
        session.getTransaction().commit();
        
        session.close();
    }
    
    public void eliminarRegistroDiario(RegistroDiario r) {
        session = HibernateUtil.getSessionFactory().openSession();
        session.getTransaction().begin();
        session.delete(r);
        session.getTransaction().commit();
        session.close();
    }
}
