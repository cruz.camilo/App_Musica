/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "archivo")
public class Archivo implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "usuario_id")
    private Long codigo;
     
    @Column
    private String recurso;
    
    @Column
    private String extension;
    
    @Column(columnDefinition = "datetime")
    private Timestamp fecha_registro;
    
    @Column
    private String descripcion;

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getRecurso() {
        return recurso;
    }

    public void setRecurso(String recurso) {
        this.recurso = recurso;
    }

    public Timestamp getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Timestamp fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
 
    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
  

    public Archivo() {
    }

    public Archivo(String recurso, String extension, String descripcion) {
        this.recurso = recurso;
        this.extension = extension;
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Archivo{" + "codigo=" + codigo + ", recurso=" + recurso + ", fecha_registro=" + fecha_registro + ", descripcion=" + descripcion + '}';
    }


}
