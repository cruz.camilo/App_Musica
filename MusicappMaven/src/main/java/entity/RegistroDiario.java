
package entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="registro_diario")
public class RegistroDiario implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "registro_id")
    private int id;
    
    @Column(columnDefinition = "datetime")
    private Timestamp fecha_registro;
    
    @Column
    private Integer bpm;
    
    @Column
    private Integer duracion; 
    
    @Column
    private String comentarios;
    
    @Column(name="nombre_fragmento")
    private String fragmentoNombre;
    
    @ManyToOne
    @JoinColumn(name = "usuario_id")
    private Usuario usuario;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
       
        
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public Integer getBpm() {
        return bpm;
    }

    public void setBpm(Integer bpm) {
        this.bpm = bpm;
    }

    public Integer getDuracion() {
        return duracion;
    }

    public void setDuracion(Integer duracion) {
        this.duracion = duracion;
    }
        

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public Timestamp getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Timestamp fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public String getFragmentoNombre() {
        return fragmentoNombre;
    }

    public void setFragmentoNombre(String fragmentoNombre) {
        this.fragmentoNombre = fragmentoNombre;
    }
    
    public RegistroDiario() {
    }

    public RegistroDiario(Integer bpm, Integer duracion, String comentarios, String fragmentoNombre, Usuario usuario) {
        this.bpm = bpm;
        this.duracion = duracion;
        this.comentarios = comentarios;
        this.fragmentoNombre = fragmentoNombre;
        this.usuario = usuario;
    }

    @Override
    public String toString() {
        return "RegistroDiario{" + "id=" + id + ", fecha_registro=" + fecha_registro + ", bpm=" + bpm + ", duracion=" + duracion + ", comentarios=" + comentarios + '}';
    }

}
