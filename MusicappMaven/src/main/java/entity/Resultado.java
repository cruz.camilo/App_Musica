/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="resultado")
public class Resultado implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "resultado_id")
    private long id;
    
    @Column
    private Integer nota;

    @Column
    private String resultado;

    @Column
    private String comentarios;
    
    @ManyToOne
    @JoinColumn(name = "evaluacion_id")
    private Evaluacion evaluacion;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getNota() {
        return nota;
    }

    public void setNota(Integer nota) {
        this.nota = nota;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }
    
    public Evaluacion getEvaluacion() {
        return evaluacion;
    }

    public void setEvaluacion(Evaluacion evaluacion) {
        this.evaluacion = evaluacion;
    }

    public Resultado() {
    }

    public Resultado(Integer nota, String resultado, String comentarios, Evaluacion evaluacion) {
        this.nota = nota;
        this.resultado = resultado;
        this.comentarios = comentarios;
        this.evaluacion = evaluacion;
    }    
  
    @Override
    public String toString() {
        return "Resultado{" + "id=" + id + ", nota=" + nota + ", resultado=" + resultado + ", comentarios=" + comentarios + '}';
    }
     
}
