/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/*POJO = Plane  Plain Old Java Object */
@Entity
@Table(name = "usuario")
public class Usuario implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "usuario_id")
    private Long codigo;

    @Column
    private String nombre;

    @Column
    private String correo;

    @Column
    private String clave;

    @Column
    private String tipo;
    
    @Column
    private String foto;
    
    @Column(name = "acerca_de_mi")
    private String acercaDeMi;
    
    @ManyToMany
    @JoinTable(name = "usuario_curso",
            joinColumns = {@JoinColumn(name = "codigo_usuario")},
            inverseJoinColumns = {@JoinColumn(name = "curso_id")})
    private List<Curso> cursos = new ArrayList<>();

    //private String perfil_profesor;

    @Column(columnDefinition = "datetime")
    private Timestamp fecha_registro;

    @OneToMany(mappedBy = "usuario", 
            cascade = CascadeType.ALL, 
            fetch = FetchType.LAZY)
    private List<Publicacion> publicaciones;
    
    @OneToMany(mappedBy = "usuario", 
            cascade = CascadeType.ALL, 
            fetch = FetchType.LAZY)
    private List<Evaluacion> evaluaciones;
    
    @OneToMany(mappedBy = "usuario", 
            cascade = CascadeType.ALL, 
            fetch = FetchType.LAZY)
    private List<Leccion> lecciones;
    
    @OneToMany(mappedBy = "usuario", 
            cascade = CascadeType.ALL, 
            fetch = FetchType.LAZY)
    private List<Fragmento> fragmentos;
    
    @OneToMany(mappedBy = "usuario", 
            cascade = CascadeType.ALL, 
            fetch = FetchType.LAZY)
    private List<RegistroDiario> registriosDiario;
    
    @OneToMany(mappedBy = "usuario", 
            cascade = CascadeType.ALL, 
            fetch = FetchType.LAZY)
    private List<EvaluacionEstudiante> evaluacionesEstudiante;

    public Usuario() {
    }

    public Usuario(String nombre, String correo, String clave, String tipo) {
        this.nombre = nombre;
        this.correo = correo;
        this.clave = clave;
        this.tipo = tipo;
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Timestamp getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Timestamp fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public List<Publicacion> getPublicaciones() {
        return publicaciones;
    }

    public void setPublicaciones(List<Publicacion> publicaciones) {
        this.publicaciones = publicaciones;
    }
    
    public List<Evaluacion> getEvaluaciones() {
        return evaluaciones;
    }

    public void setEvaluaciones(List<Evaluacion> evaluaciones) {
        this.evaluaciones = evaluaciones;
    }
    
    public List<Leccion> getLecciones() {
        return lecciones;
    }

    public void setLecciones(List<Leccion> lecciones) {
        this.lecciones = lecciones;
    }
    
    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
    
    public String getAcercaDeMi() {
        return acercaDeMi;
    }

    public void setAcercaDeMi(String acercaDeMi) {
        this.acercaDeMi = acercaDeMi;
    }

    public List<Curso> getCursos() {
        return cursos;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

    public List<Fragmento> getFragmentos() {
        return fragmentos;
    }

    public void setFragmentos(List<Fragmento> fragmentos) {
        this.fragmentos = fragmentos;
    }
         
    public List<RegistroDiario> getRegistriosDiario() {
        return registriosDiario;
    }

    public void setRegistriosDiario(List<RegistroDiario> registriosDiario) {
        this.registriosDiario = registriosDiario;
    }
 
    public List<EvaluacionEstudiante> getEvaluacionesEstudiante() {
        return evaluacionesEstudiante;
    }

    public void setEvaluacionesEstudiante(List<EvaluacionEstudiante> evaluacionesEstudiante) {
        this.evaluacionesEstudiante = evaluacionesEstudiante;
    }

    @Override
    public String toString() {
        return "Usuario{" + "codigo=" + codigo + ", nombre=" + nombre + ", correo=" + correo + ", clave=" + clave + ", tipo=" + tipo + ", foto=" + foto + ", acercaDeMi=" + acercaDeMi + ", cursos=" + cursos + ", fecha_registro=" + fecha_registro + '}';
    }    

}