/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author JUANCHO
 */
@Entity
@Table(name = "leccion")
public class Leccion implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "leccion_id")
    private long id;
    
    @Column
    private String titulo;
    
    @Column
    private String tema;
    
    @Column
    private int dificultad;
     
    @Column
    private String resumen;
    
    @Column(columnDefinition = "TEXT")
    private String contenido;
    
    @Column
    private String instrumento;
    
    @Column(name = "fecha_publicacion", columnDefinition = "datetime")
    private Timestamp fechaPublicacion;
    
    @ManyToOne
    @JoinColumn(name = "usuario_id")
    private Usuario usuario;
    
    
    // Titulo, tema, resumen, dificultad, instrumento, contenido

    public Leccion() {
    }

    public Leccion(String titulo, String tema, int dificultad, String resumen, String contenido, String instrumento, Usuario usuario) {
        this.titulo = titulo;
        this.tema = tema;
        this.dificultad = dificultad;
        this.resumen = resumen;
        this.contenido = contenido;
        this.instrumento = instrumento;
        this.usuario = usuario;
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getResumen() {
        return resumen;
    }

    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    public int getDificultad() {
        return dificultad;
    }

    public void setDificultad(int dificultad) {
        this.dificultad = dificultad;
    }

    public String getInstrumento() {
        return instrumento;
    }

    public void setInstrumento(String instrumento) {
        this.instrumento = instrumento;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public Timestamp getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Timestamp fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }
 
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public String toString() {
        return "Leccion{" + "id=" + id + ", titulo=" + titulo + ", tema=" + tema + ", dificultad=" + dificultad + ", resumen=" + resumen + ", contenido=" + contenido + ", instrumento=" + instrumento + ", fechaPublicacion=" + fechaPublicacion + ", usuario=" + usuario + '}';
    }
}
