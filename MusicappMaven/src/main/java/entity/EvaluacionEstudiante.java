/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "evaluacion_envio")
public class EvaluacionEstudiante implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "evaluacion_estudiante_id")
    private Long codigo;

    @Column
    private String audio;
    
    @Column(columnDefinition = "datetime")
    private Timestamp fecha_registro;
    
    @Column
    private String comentario;
    
    @ManyToOne
    @JoinColumn(name = "estudiante_id")
    private Usuario usuario;
    
    /*@ManyToOne
    @JoinColumn(name = "evaluacion_id")
    private Evaluacion evaluacion;*/

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public Timestamp getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Timestamp fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public EvaluacionEstudiante() {
    }

    public EvaluacionEstudiante(String audio, String comentario, Usuario usuario) {
        this.audio = audio;
        this.comentario = comentario;
        this.usuario = usuario;
    }

    @Override
    public String toString() {
        return "Archivo{" + "codigo=" + codigo + ", audio=" + audio + ", fecha_registro=" + fecha_registro + ", comentario=" + comentario + '}';
    }       

}
