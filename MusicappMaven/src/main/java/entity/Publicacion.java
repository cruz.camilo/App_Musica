/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "publicacion")
public class Publicacion implements Serializable {
    @Id
    @Column(name = "publicacion_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long codigo;

    @Column(name = "contenido", columnDefinition = "TEXT")
    
    private String contenido;
    
    @Column(name = "fecha_registro", columnDefinition = "DATETIME")
    private Timestamp fechaPublicacion;
    
    @Column
    private String imagen;

    @ManyToOne
    @JoinColumn(name = "usuario_id")
    private Usuario usuario;

    public Publicacion() {
    }

    public Publicacion(String contenido, Usuario usuario) {
        this.contenido = contenido;
        this.usuario = usuario;
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Timestamp getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Timestamp fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    @Override
    public String toString() {
        return "Publicacion{" + "codigo=" + codigo + ", contenido=" + contenido + ", usuario=" + usuario + '}';
    }            
}
