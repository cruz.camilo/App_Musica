
package bean;

import entity.RegistroDiario;
import entity.Usuario;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import model.RegistroDiarioModel;

@ManagedBean
@RequestScoped
public class RegistroDiarioBean {
    private RegistroDiario registroDiario = new RegistroDiario();
    private String mensajeSalida;
    RegistroDiarioModel rmodel;
    
    private List<RegistroDiario> registrosDiario;

    public RegistroDiario getRegistroDiario() {
        return registroDiario;
    }

    public void setRegistroDiario(RegistroDiario registroDiario) {
        this.registroDiario = registroDiario;
    }

    public String getMensajeSalida() {
        return mensajeSalida;
    }

    public void setMensajeSalida(String mensajeSalida) {
        this.mensajeSalida = mensajeSalida;
    }

    @PostConstruct
    public void posConstruct() {
        rmodel = new RegistroDiarioModel();
    }

    public String registrarRegistroDiario() {
        registroDiario.setUsuario(
                (Usuario) FacesContext.
                getCurrentInstance().
                getExternalContext()
                .getSessionMap().
                get("udentro"));
        rmodel.registrarRegistroDiario(registroDiario);
        return "practicar";
    }
    
    public List listarRegistrosDiario() {
        return rmodel.listarRegistroDiario();
    }
}
