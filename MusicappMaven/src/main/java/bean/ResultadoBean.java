package bean;

import entity.Resultado;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import model.ResultadoModel;

@ManagedBean
@RequestScoped
public class ResultadoBean {

    private Resultado resultado = new Resultado();
    private String mensajeSalida;
    ResultadoModel pmodel;
    
    private List<Resultado> resultados;

    public Resultado getResultado() {
        return resultado;
    }

    public void setResultado(Resultado resultado) {
        this.resultado = resultado;
    }

    public String getMensajeSalida() {
        return mensajeSalida;
    }

    public void setMensajeSalida(String mensajeSalida) {
        this.mensajeSalida = mensajeSalida;
    }

    @PostConstruct
    public void posConstruct() {
        pmodel = new ResultadoModel();
    }

    public String registrarResultado() {      
        pmodel.registrarResultado(resultado);
        return "index";
    }
    
    public List listarResultadoes() {
        return pmodel.listarResultadoes();
    }

}
