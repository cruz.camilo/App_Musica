package bean;

import model.UsuarioModel;
import entity.Usuario;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import java.io.Serializable;
import java.nio.file.Files;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;

@ManagedBean
@RequestScoped
public class UsuarioBean implements Serializable {

    private Usuario usuario = new Usuario();
    private Part foto;
    private String mensajeSalida;
    UsuarioModel usuDao = null;

    public String getMensajeSalida() {
        return mensajeSalida;
    }

    public void setMensajeSalida(String mensajeSalida) {
        this.mensajeSalida = mensajeSalida;
    }

    public Part getFoto() {
        return foto;
    }

    public void setFoto(Part foto) {
        this.foto = foto;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @PostConstruct
    public void postConstructor() {
        this.usuDao = new UsuarioModel();
    }

    private List<Usuario> usuarios = new ArrayList<>();

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public String verificarDatos() throws Exception {
        UsuarioModel usuDAO = new UsuarioModel();
        Usuario us;
        String resultado;

        try {
            us = usuDAO.verificarDatos(this.usuario);
            if (us != null) {

                FacesContext.getCurrentInstance().getExternalContext()
                        .getSessionMap().put("udentro", us);

                resultado = "index";
            } else {
                resultado = "register";
            }
        } catch (Exception e) {

            throw e;
        }

        return resultado;
    }

    public String registrarUsuario() {

        usuDao.registrarUsuario(usuario);

        FacesContext.getCurrentInstance().getExternalContext()
                .getSessionMap().put("udentro", this.usuario);

        return "index";

    }

    public String actualizarUsuario() {
        Usuario uSession = (Usuario) FacesContext.getCurrentInstance().getExternalContext()
                .getSessionMap().get("udentro");

        if (foto != null) {
            ExternalContext tmpEC;

            tmpEC = FacesContext.getCurrentInstance().getExternalContext();
            String path = tmpEC.getRealPath("/");

            path = path.substring(0, path.indexOf("target"));

            path += "src/main/webapp/resources/images/profile/";

            String nombreFinalFoto = System.nanoTime() + ".jpg";

            try (InputStream input = foto.getInputStream()) {
                Files.copy(input, new File(path, nombreFinalFoto).toPath());
            } catch (IOException e) {

            }

            uSession.setFoto(nombreFinalFoto);
        }

        
        usuDao.updateUser(uSession);
        /*
        FacesContext.getCurrentInstance().getExternalContext()
                .getSessionMap().put("udentro", );
         */
        return "index";
    }

    public String cerrarSesion() {
        FacesContext.getCurrentInstance().getExternalContext()
                .invalidateSession();
        return "register";
    }
}
