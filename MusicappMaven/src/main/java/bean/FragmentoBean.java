/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import entity.Fragmento;
import entity.Usuario;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import model.FragmentoModel;

@ManagedBean
@RequestScoped
public class FragmentoBean {

    private Fragmento fragmento = new Fragmento();
    private List<Fragmento> fragmentos = new ArrayList();
    FragmentoModel fmodel;
    
    public Fragmento getFragmento() {
        return fragmento;
    }

    public void setFragmento(Fragmento fragmento) {
        this.fragmento = fragmento;
    }
    
     @PostConstruct
    public void posConstruct() {
        fmodel = new FragmentoModel();
    }

    public String registrarFragmento() {
    fragmento.setUsuario(
                (Usuario) FacesContext.
                getCurrentInstance().
                getExternalContext()
                .getSessionMap().
                get("udentro"));
        fmodel.registrarFragmento(fragmento);
        return "practicar";
    }

    public List listarFragmentos() {        
        return fmodel.listarFragmento();
    }
      
    
}
