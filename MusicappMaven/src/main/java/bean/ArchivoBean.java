package bean;

import model.ArchivoModel;
import entity.Archivo;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import java.io.Serializable;
import java.nio.file.Files;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;

@ManagedBean
@RequestScoped
public class ArchivoBean implements Serializable {

    private Archivo archivo = new Archivo();
    private Part recurso;
    private String mensajeSalida;
    ArchivoModel arcDao = null;

    public Part getRecurso() {
        return recurso;
    }

    public void setRecurso(Part recurso) {
        this.recurso = recurso;
    }

    public Archivo getArchivo() {
        return archivo;
    }

    public void setArchivo(Archivo archivo) {
        this.archivo = archivo;
    }

    @PostConstruct
    public void postConstructor() {
        this.arcDao = new ArchivoModel();
    }

    private List<Archivo> archivos = new ArrayList<>();

    public List<Archivo> getArchivos() {
        return archivos;
    }

    public void setArchivos(List<Archivo> archivos) {
        this.archivos = archivos;
    }

    public String registrarImagen() {
        
        if (recurso != null) {
            ExternalContext tmpEC;

            tmpEC = FacesContext.getCurrentInstance().getExternalContext();
            String path = tmpEC.getRealPath("/");

            path = path.substring(0, path.indexOf("target"));

            path += "src/main/webapp/resources/mis_recursos/";
            
            
            String nombreFinalRecurso = System.nanoTime() + "." + "mp3";

            try (InputStream input = recurso.getInputStream()) {
                Files.copy(input, new File(path, nombreFinalRecurso).toPath());
            } catch (IOException e) {

            }
            archivo.setExtension(getFileExtension(recurso));
            archivo.setRecurso(nombreFinalRecurso);
        }
        arcDao.registrarArchivo(archivo);
        FacesContext.getCurrentInstance().getExternalContext()
                .getSessionMap().put("arcdentro", this.archivo);

        return "recursos_archivos";
    }

    public String actualizarArchivo() {

        return "recursos_archivos";
    }
    
    public List listarArchivos() {
        return arcDao.listarArchivo();
    }

    public static String getFileExtension(Part file) {
        String fileExtension = "";
        String fileName = file.getName();
        if (fileName.contains(".") && fileName.lastIndexOf(".") != 0) {
            fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1);
        }
        return fileExtension;
    }
}
