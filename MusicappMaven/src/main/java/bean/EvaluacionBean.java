/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import entity.Evaluacion;
import model.EvaluacionModel;
import entity.Usuario;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;


@ManagedBean
@RequestScoped
public class EvaluacionBean {
    
    private Evaluacion evaluacion = new Evaluacion();
    //private String mensajeSalida;
    EvaluacionModel emodel;
    
    private List<Evaluacion> evaluaciones;
        
    public Evaluacion getEvaluacion() {
        return evaluacion;
    }
    public void setEvaluacion(Evaluacion evaluacion) {
        this.evaluacion = evaluacion;
    }

    /*public String getMensajeSalida() {
        return mensajeSalida;
    }

    public void setMensajeSalida(String mensajeSalida) {
        this.mensajeSalida = mensajeSalida;
    }*/

    @PostConstruct
    public void posConstruct() {
        emodel = new EvaluacionModel();
    }

    public String registrarEvaluacion() {
        evaluacion.setUsuario(
                (Usuario) FacesContext.
                getCurrentInstance().
                getExternalContext()
                .getSessionMap().
                get("udentro"));
        emodel.registrarEvaluacion(evaluacion);
        return "mis_evaluaciones";
    }
    
    public List listarEvaluaciones() {
        return emodel.listarEvaluaciones();
    }

}
