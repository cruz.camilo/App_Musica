package bean;

import static bean.ArchivoBean.getFileExtension;
import entity.Publicacion;
import entity.Usuario;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;
import model.PublicacionModel;

@ManagedBean
@RequestScoped
public class PublicacionBean {

    private Publicacion publicacion = new Publicacion();
    private String mensajeSalida;
    private Part imagen;
    PublicacionModel pmodel;
    
    private List<Publicacion> publicaciones;

    public Publicacion getPublicacion() {
        return publicacion;
    }

    public void setPublicacion(Publicacion publicacion) {
        this.publicacion = publicacion;
    }

    public String getMensajeSalida() {
        return mensajeSalida;
    }

    public void setMensajeSalida(String mensajeSalida) {
        this.mensajeSalida = mensajeSalida;
    }

    public Part getImagen() {
        return imagen;
    }

    public void setImagen(Part imagen) {
        this.imagen = imagen;
    }

    @PostConstruct
    public void posConstruct() {
        pmodel = new PublicacionModel();
    }

    public String registrarPublicacion() {
        publicacion.setUsuario(
                (Usuario) FacesContext.
                getCurrentInstance().
                getExternalContext()
                .getSessionMap().
                get("udentro"));
        
        if (imagen != null) {
            ExternalContext tmpEC;

            tmpEC = FacesContext.getCurrentInstance().getExternalContext();
            String path = tmpEC.getRealPath("/");

            path = path.substring(0, path.indexOf("target"));

            path += "src/main/webapp/resources/mis_recursos/publicaciones";
            
            
            String nombreFinalRecurso = System.nanoTime() + "." + "jpg";

            try (InputStream input = imagen.getInputStream()) {
                Files.copy(input, new File(path, nombreFinalRecurso).toPath());
            } catch (IOException e) {

            }
            
        publicacion.setImagen(nombreFinalRecurso);
        }        
        FacesContext.getCurrentInstance().getExternalContext()
                .getSessionMap().put("pub", this.publicacion);
        pmodel.registrarPublicacion(publicacion);
        
        return "index";
    }
    
    public List listarPublicaciones() {
        return pmodel.listarPublicaciones();
    }

}
