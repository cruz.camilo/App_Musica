/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import entity.Leccion;
import entity.Usuario;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import model.LeccionModel;

@ManagedBean
@RequestScoped
public class LeccionBean {
    private Leccion leccion = new Leccion();
    //private String mensajeSalida;
    LeccionModel emodel;
    
    private List<Leccion> lecciones;
    
    public Leccion getLeccion() {
        return leccion;
    }
    
    public void setLeccion(Leccion leccion) {
        this.leccion = leccion;
    }

    /*public String getMensajeSalida() {
        return mensajeSalida;
    }

    public void setMensajeSalida(String mensajeSalida) {
        this.mensajeSalida = mensajeSalida;
    }*/

    @PostConstruct
    public void posConstruct() {
        emodel = new LeccionModel();
    }

    public String registrarLeccion() {
        leccion.setUsuario(
                (Usuario) FacesContext.
                getCurrentInstance().
                getExternalContext()
                .getSessionMap().
                get("udentro"));
        emodel.registrarLeccion(leccion);
        return "lecciones";
    }
    
    public List listarLecciones() {
        return emodel.listarLecciones();
    }

}
