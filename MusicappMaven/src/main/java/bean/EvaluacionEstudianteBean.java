package bean;


import entity.EvaluacionEstudiante;
import entity.Usuario;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import java.io.Serializable;
import java.nio.file.Files;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;
import model.EvaluacionEstudianteModel;

@ManagedBean
@RequestScoped
public class EvaluacionEstudianteBean implements Serializable {

    private EvaluacionEstudiante evaluacion = new EvaluacionEstudiante();
    private Part audio;
    
    EvaluacionEstudianteModel eDao = null;

    public Part getAudio() {
        return audio;
    }

    public void setAudio(Part audio) {
        this.audio = audio;
    }

    public EvaluacionEstudiante getEvaluacion() {
        return evaluacion;
    }

    public void setEvaluacion(EvaluacionEstudiante evaluacion) {
        this.evaluacion = evaluacion;
    }
   

    @PostConstruct
    public void postConstructor() {
        this.eDao = new EvaluacionEstudianteModel();
    }

    private List<EvaluacionEstudiante> evaluaciones = new ArrayList<>();

    public List<EvaluacionEstudiante> getEvaluaciones() {
        return evaluaciones;
    }

    public void setEvaluaciones(List<EvaluacionEstudiante> evaluaciones) {
        this.evaluaciones = evaluaciones;
    }

    public String registrarEvaluacionEstudiante() {
         evaluacion.setUsuario(
                (Usuario) FacesContext.
                getCurrentInstance().
                getExternalContext()
                .getSessionMap().
                get("udentro"));
         
        if (audio != null) {
            ExternalContext tmpEC;

            tmpEC = FacesContext.getCurrentInstance().getExternalContext();
            String path = tmpEC.getRealPath("/");

            path = path.substring(0, path.indexOf("target"));

            path += "src/main/webapp/resources/mis_recursos/";
            
            
            String nombreFinalAudio = System.nanoTime() + "." + "mp3";

            try (InputStream input = audio.getInputStream()) {
                Files.copy(input, new File(path, nombreFinalAudio).toPath());
            } catch (IOException e) {

            }
            
            evaluacion.setAudio(nombreFinalAudio);
        }
        eDao.registrarEvaluacion(evaluacion);
        FacesContext.getCurrentInstance().getExternalContext()
                .getSessionMap().put("audentro", this.evaluacion);

        return "mis_calificaciones";
    }

    public String actualizarArchivo() {

        return "recursos_archivos";
    }
    
    public List listarEvaluaciones() {
        return eDao.listarEvaluacion();
    }

    public static String getFileExtension(Part file) {
        String fileExtension = "";
        String fileName = file.getName();
        if (fileName.contains(".") && fileName.lastIndexOf(".") != 0) {
            fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1);
        }
        return fileExtension;
    }


}
