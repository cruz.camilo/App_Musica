/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import entity.Curso;
import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import javax.annotation.PostConstruct;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import model.CursoModel;

@ManagedBean
@RequestScoped
public class CursoBean implements Serializable {

    private Curso curso = new Curso();
    CursoModel cmodel;

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }
    
     @PostConstruct
    public void posConstruct() {
        cmodel = new CursoModel();
    }

    public String registrarCurso() {
        CursoModel usuDao = new CursoModel();

        usuDao.registrarCurso(curso);

        return "crear_curso";

    }

    private List<Curso> cursos = new ArrayList();

    public List<Curso> getCursos() {
        return cursos;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

    public List consultarCursos() {        
        return cmodel.listarCurso();
    }
}