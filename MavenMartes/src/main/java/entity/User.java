/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author JUANCHO
 */
@Entity
@Table(name = "users")

public class User implements Serializable {
    
    private static Long serialVersionUDI = 1L;
    // Para que una clase sea java beans debe tener:
    
    // Primero encapsulacion (Setter's y Getter's)
    // Un constructor vacio
    // Un constructor full
    // Sobre escritura del toString
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
    private Long code;
    
    @Column
    private String name;
    
    @Column(name = "last_name")
    private String lastName;

    public User() {
    }

    public User(String name, String lastName) {
        
        this.name = name;
        this.lastName = lastName;
    }

    /**
     * @return the code
     */
    
    public Long getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(Long code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "User{" + "code=" + code + ", name=" + name + ", lastName=" + lastName + '}';
    }    
}