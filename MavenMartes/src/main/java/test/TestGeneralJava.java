/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import entity.User;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author JUANCHO
 */
public class TestGeneralJava {
    
    private static EntityManager manager;
    private static EntityManagerFactory emf;
    
    
    
    public static void main(String[] args) {
        // Tres transferendcias para instanciar EntityManager        
        emf = Persistence.createEntityManagerFactory("Persistence1");
        
        manager = emf.createEntityManager();        
        
        manager.getTransaction().begin(); // Establece que la linea de transacciones va a comenzar
        /*Principio de Create*/
        User u = new User("Luisa", "Urazan");
        manager.persist(u);
        
        /*Principio de Read Uno*/
        User u_de_base_de_datos;
        u_de_base_de_datos = manager.find(User.class, 3L);
        System.out.println(u_de_base_de_datos);
       /*Fin de Read UNO*/
       
       /* Principio de Update */
       
       User usuario;
       usuario = manager.find(User.class, 2L);
       usuario.setName("Eduardo");
        System.out.println(usuario);

        // Pare borrar registros
        u = manager.find(User.class, 1L);
        manager.remove(u);
        
        // Acciones a la base de datos
        
       // Crear la tabla (Esquema de la base de datos)
       // Guardar el registro ¿En dónde? Guardar en tabla
       
        manager.getTransaction().commit();
        // Finalice el manager entity-libera memoria
        manager.close();
    }
}